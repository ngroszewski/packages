# packages

Install common packages

## Role Variables

* `install`
  * Type: Array of strings
  * Usages: List of packages to install

* `remove`: Array of string
  * Type: Array of strings
  * Usages: List of packages to remove

* `cask_install`
  * Type: Array of strings
  * Usages: List of homebrew casks to install
  * OS: OSX

* `cask_remove`: Array of string
  * Type: Array of strings
  * Usages: List of homebrew casks to remove
  * OS: OSX

```
packages:
  install:
    - coreutils
    - git
  cask_install:
    - xquartz
```

## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.packages

## License

MIT
